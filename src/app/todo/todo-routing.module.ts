import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserTodoComponent } from './user-todo/user-todo.component';

const routes: Routes = [
  { path: ':id', component: UserTodoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoRoutingModule { }
