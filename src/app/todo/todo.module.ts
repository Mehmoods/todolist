import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserTodoComponent } from './user-todo/user-todo.component';
import { TodoRoutingModule } from './todo-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        UserTodoComponent
    ],
    imports: [
        CommonModule,
        TodoRoutingModule,
        SharedModule
    ]
})

export class TodoModule { }
