import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetTodos, GetTodosById } from '../state/todo.action';
import { Todo } from '../shared/models/todo.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  cols: { field: string }[];
  todos: Todo[] | HttpErrorResponse;
  hasError = false;
  loader = true;
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new GetTodos());
    this.store.select(state => state)
      .subscribe(data => {
        this.todos = data.todos.todoList;
        this.hasError = data.todos.hasError;
        setTimeout(() => this.loader = false, 500);
      });

    this.cols = [
      { field: 'userName' },
      { field: 'description' },
      { field: 'completed' }
    ];
  }

}
