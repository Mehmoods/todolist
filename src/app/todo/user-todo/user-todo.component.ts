import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Todo } from 'src/app/shared/models/todo.model';
import { ActivatedRoute } from '@angular/router';
import { GetTodosById } from 'src/app/state/todo.action';

@Component({
  selector: 'app-user-todo',
  templateUrl: './user-todo.component.html',
  styleUrls: ['./user-todo.component.css']
})
export class UserTodoComponent implements OnInit {
  userTodo: Todo[];
  loader = false;
  constructor(private store: Store, private route: ActivatedRoute) { }

  ngOnInit() {
    this.store.dispatch(new GetTodosById(+this.route.snapshot.params.id));
    this.store.select(state => state).subscribe(data => {
      this.userTodo = data.todos.todoList;
      setTimeout(() => this.loader = true, 800);
    });
  }

}
