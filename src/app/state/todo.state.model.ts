import { Todo } from '../shared/models/todo.model';

export class TodoStateModel {
    constructor(
        public todoList: Todo[],
        public hasError?: boolean
    ) {}
}
