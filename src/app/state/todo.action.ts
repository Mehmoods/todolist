import { Todo } from '../shared/models/todo.model';

export class GetTodos {
    static readonly type = '[Todo] Get';
}

export class GetTodosById {
    static readonly type = '[Todo] Get todo by id';
    constructor(public id: number) { }
}

export class AddTodo {
    static readonly type = '[Todo] Add todo';
    constructor(public payload: Todo) { }
}
