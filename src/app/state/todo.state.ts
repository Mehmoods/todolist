import { State, Selector, StateContext, Action } from '@ngxs/store';
import { TodoStateModel } from './todo.state.model';
import { TodoService } from '../shared/services/todo.service';
import { GetTodos, GetTodosById, AddTodo } from './todo.action';

@State<TodoStateModel>({
    name: 'todos',
    defaults: {
        todoList: [],
        hasError: false
    }
})
export class TodoState {
    constructor(private todoService: TodoService) { }

    @Selector()
    static GetTodoList(state: TodoStateModel) {
        return state;
    }

    @Action(GetTodos)
    getTodos(ctx: StateContext<TodoStateModel>) {
        return this.todoService.getTodos()
            .subscribe(
                (todoList) => {
                    ctx.setState({
                        todoList,
                        hasError: false
                    });
                },
                (error) => {
                    const errorHandler = { ...error };
                    ctx.setState({
                        todoList: [errorHandler],
                        hasError: true
                    });
                });
    }

    @Action(GetTodosById)
    getTodosById(ctx: StateContext<TodoStateModel>, { id }: GetTodosById) {
        return this.todoService.getTodosById(id)
            .subscribe((todo) => {
                ctx.setState({
                    todoList: [
                        todo
                    ]
                });
            });
    }

    @Action(AddTodo)
    addTodo(ctx: StateContext<TodoStateModel>, { payload }: AddTodo) {
        return this.todoService.addTodo(payload)
            .subscribe(
                (todo) => {
                    const state = ctx.getState();
                    ctx.patchState({
                        todoList: [
                            ...state.todoList,
                            todo
                        ]
                    });
                });
    }
}
