import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AddTodo } from 'src/app/state/todo.action';
import { Message } from 'primeng/components/common/api';
import { DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
  addTodoForm: FormGroup;
  msgs: Message[] = [];
  constructor(
    private fb: FormBuilder,
    private store: Store,
    public ref: DynamicDialogRef
  ) { }

  ngOnInit() {
    this.createForm();
    // console.log(this.ref);
    // console.log(this.config);
  }

  createForm() {
    this.addTodoForm = this.fb.group({
      userId: [
        '',
        [
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(11)
        ]
      ],
      userName: ['', [Validators.required]],
      description: ['', [Validators.required]],
      completed: [false]
    });
  }

  submitAddTodoForm() {
    this.store.dispatch(new AddTodo(this.addTodoForm.value));
    this.addTodoForm.reset();
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order submitted' });
    setTimeout(() => {
      this.msgs = [];
      this.ref.close();
    }, 5000);
  }

}
