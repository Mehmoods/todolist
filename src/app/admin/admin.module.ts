import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicDialogRef } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/api';

@NgModule({
    declarations: [
        AdminComponent,
        AddTodoComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AdminRoutingModule,
        SharedModule
    ],
    providers: [
        DynamicDialogRef, DynamicDialogConfig
    ],
    entryComponents: [
        AddTodoComponent
    ]
})
export class AdminModule { }
