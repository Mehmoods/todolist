import { Component, OnInit, ViewChild, ElementRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { Todo } from '../shared/models/todo.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngxs/store';
import { GetTodos } from '../state/todo.action';
import { AddTodoComponent } from './add-todo/add-todo.component';
import { DialogService } from 'primeng/api';
import { DynamicDialogRef } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [DialogService]
})
export class AdminComponent implements OnInit {
  constructor(
    private dialogService: DialogService,
    private store: Store,
    private ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private translate: TranslateService
  ) { }
  cols: { field: string }[];
  todos: Todo[] | HttpErrorResponse;
  hasError = false;
  loader = true;
  display = false;

  ngOnInit() {
    this.store.dispatch(new GetTodos());
    this.store.select(state => state)
      .subscribe(data => {
        this.todos = data.todos.todoList;
        this.hasError = data.todos.hasError;
        setTimeout(() => this.loader = false, 500);
      });

    this.cols = [
      { field: 'id' },
      { field: 'userId' },
      { field: 'userName' },
      { field: 'description' },
      { field: 'completed' }
    ];
  }


  showDialog() {
    this.translate.get('TODO')
      .subscribe((value) => {
        this.dialogService.open(AddTodoComponent, {
          data: {
            id: 'AddDialog'
          },
          header: value.add,
          width: '70%'
        });
      });
  }

  onTodoEditInit(todo) {
    console.log(todo);
  }

  onTodoEditSave(todo, completed) {
    console.log(todo);
    console.log(completed);
  }

  onTodoEditCancel(todo, ri) {
    console.log(todo);
    console.log(ri);
  }
}
