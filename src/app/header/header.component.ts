import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  languages: SelectItem[];
  selectedLanguage = 'en';
  isAdmin = false;
  constructor(
    public translate: TranslateService,
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (event.url === '/admin') {
          this.isAdmin = true;
        } else {
          this.isAdmin = false;
        }
      }
    });
    const language = localStorage.getItem('language');
    if (!language) {
      this.translate.setDefaultLang('en');
    }
    this.translate.setDefaultLang(language);
    this.selectedLanguage = language;
    this.languages = [
      { label: 'en', value: 'en' },
      { label: 'ru', value: 'ru' }
    ];
  }

  changeLanguage(event) {
    localStorage.setItem('language', event.value);
    this.translate.setDefaultLang(event.value);
  }

}
