import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  url = `http://localhost:3200/todos`;
  constructor(private http: HttpClient) { }

  getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.url);
  }

  getTodosById(id): Observable<Todo> {
    return this.http.get<Todo>(`${this.url}/${id}`);
  }

  addTodo(todo): Observable<Todo> {
    const httpOptions = {headers: this.headers};
    return this.http.post<Todo>(this.url, todo, httpOptions);
  }
}
