import { NgModule } from '@angular/core';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { DynamicDialogModule } from 'primeng/dynamicdialog';

@NgModule({
  imports: [
    DropdownModule,
    TableModule,
    SliderModule,
    MultiSelectModule,
    ProgressSpinnerModule,
    ButtonModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    DynamicDialogModule
  ],
  exports: [
    DropdownModule,
    TableModule,
    SliderModule,
    MultiSelectModule,
    ProgressSpinnerModule,
    ButtonModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    DynamicDialogModule
  ]
})

export class PrimeModule { }
