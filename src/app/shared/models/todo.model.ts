export class Todo {
    constructor(
        public id: number,
        public userId: string,
        public userName: string,
        public description: string,
        public completed: boolean
    ) { }
}
