import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { PrimeModule } from './prime.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
   LoaderComponent
  ],
  imports: [
    CommonModule,
    PrimeModule,
    TranslateModule
  ],
  exports: [
    LoaderComponent,
    PrimeModule,
    TranslateModule
  ]
})
export class SharedModule { }
